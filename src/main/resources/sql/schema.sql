CREATE TABLE IF NOT EXISTS employees(
    id BIGINT AUTO_INCREMENT NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    employeeType VARCHAR(255) NOT NULL,
    isActive BIT DEFAULT 1,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS timeRecord(
    id BIGINT AUTO_INCREMENT NOT NULL,
    timeIn TIME(0) NOT NULL,
    timeOut TIME(0) NOT NULL,
    isUpdated BIT DEFAULT 1,
    date DATE,
    employeeId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY(employeeId) REFERENCES employees(id)
);

