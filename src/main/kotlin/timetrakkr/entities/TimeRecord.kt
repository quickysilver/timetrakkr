package timetrakkr.entities

import java.sql.Date
import java.sql.Time
import javax.persistence.*

@Entity(name = "TimeRecord")
@Table(name = "timeRecord")
data class TimeRecord(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(
        nullable = false,
        updatable = true,
        name = "timeIn"
    )
    var timeIn: Time,

    @Column(
        nullable = false,
        updatable = true,
        name = "timeOut"
    )
    var timeOut: Time,

    @Column(
        nullable = false,
        updatable = true,
        name = "isUpdated"
    )

    var isUpdated: Boolean = true,
    @Column(
        nullable = false,
        updatable = true,
        name = "date"
    )
    var date: Date,

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    var employee: Employee? = null
)
