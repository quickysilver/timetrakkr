package timetrakkr.service

import timetrakkr.entities.TimeRecord

interface TimeRecordService {
    fun addTimeRecord(body: TimeRecord, employeeId: Long): TimeRecord
    fun getTimeRecordById(id: Long): TimeRecord
    fun updateTimeRecord(body: TimeRecord, id: Long): TimeRecord
}