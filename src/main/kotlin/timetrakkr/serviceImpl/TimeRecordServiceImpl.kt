package timetrakkr.serviceImpl


import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import timetrakkr.entities.TimeRecord
import timetrakkr.repositories.EmployeeRepository
import timetrakkr.repositories.TimeRecordRepository
import timetrakkr.service.TimeRecordService


@Service
class TimeRecordServiceImpl(
    private val repository: TimeRecordRepository,
    private val employeeRepository: EmployeeRepository
): TimeRecordService {
    override fun addTimeRecord(body: TimeRecord, employeeId: Long): TimeRecord {
        val employee = employeeRepository.findById(employeeId).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $employeeId does not exist!")
        }
        return repository.save(body.copy(employee = employee))
    }

    override fun getTimeRecordById(id: Long): TimeRecord {
        val timeRecord = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Time Record with Id: $id does not exist.")
        }

        return timeRecord
    }

    override fun updateTimeRecord(body: TimeRecord, id: Long): TimeRecord {
        val timeRecord = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Time Record with Id: $id does not exist.")
        }

        return repository.save(
            timeRecord.copy(
                timeIn = body.timeIn,
                timeOut = body.timeOut,
                date = body.date
            )
        )
    }
}