package timetrakkr.handler

import org.springframework.web.bind.annotation.*
import timetrakkr.entities.TimeRecord
import timetrakkr.service.TimeRecordService

class TimeRecordHandler(
    private val timeRecordService: TimeRecordService
) {
    @PostMapping ("/api/timerecords/{employeeId}/")
    fun addTimeRecord(@RequestBody body: TimeRecord, @PathVariable("employeeId") employeeId: Long): TimeRecord = timeRecordService.addTimeRecord(body, employeeId)

    @GetMapping("/api/timerecord/{id}/")
    fun getTimeRecordById(@PathVariable("id") id: Long) = timeRecordService.getTimeRecordById(id)

    @PutMapping("/api/timerecord/{id}/")
    fun updateTimeRecord(@RequestBody body: TimeRecord, @PathVariable id: Long) = timeRecordService.updateTimeRecord(body, id)
}