package timetrakkr.entities

import javax.persistence.*

@Entity(name = "Employee")
@Table(name = "employees")
data class Employee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "employeeType"
    )
    var employeeType: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true

)
