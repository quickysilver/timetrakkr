package timetrakkr.handler

import org.springframework.web.bind.annotation.*
import timetrakkr.entities.Employee
import timetrakkr.service.EmployeeService

@RestController
class EmployeeHandler(
    private val employeeService: EmployeeService
)
{
    @PostMapping("/api/employees")
    fun createEmployee(@RequestBody body: Employee):Employee = employeeService.createEmployee(body)

    @PutMapping("/api/employee/{id}/")
    fun updateEmployee(@RequestBody body: Employee, @PathVariable("id") id: Long): Employee = employeeService.updateEmployee(body, id)

    @PutMapping("/api/employees/{id}/")
    fun activateEmployee(@RequestBody body: Employee, @PathVariable("id") id:Long): Employee = employeeService.activateEmployee(body, id)

}