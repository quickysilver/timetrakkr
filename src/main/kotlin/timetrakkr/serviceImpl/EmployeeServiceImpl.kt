package timetrakkr.serviceImpl


import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import timetrakkr.entities.Employee
import timetrakkr.repositories.EmployeeRepository
import timetrakkr.service.EmployeeService
import timetrakkr.utils.EmployeeTypeEnum

@Service
class EmployeeServiceImpl(
    private val repository: EmployeeRepository
) : EmployeeService {
    override fun createEmployee(body: Employee): Employee {
        if(repository.doesEmployeeExist(body.firstName, body.lastName)){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Employee ${body.firstName}, ${body.lastName} already exists!")
        }
        val employeetype = EmployeeTypeEnum.valueOf(body.employeeType.uppercase())
        return repository.save(
            body.copy(
                employeeType = employeetype.value
            )
        )
    }

    override fun updateEmployee(body: Employee, id: Long): Employee {
        val employee = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist!")
        }
        return repository.save(
            employee.copy(
                firstName = body.firstName,
                lastName = body.lastName

            )
        )

    }

    override fun activateEmployee(body: Employee, id: Long): Employee {
        val employee = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist!")
        }
        return repository.save(
            employee.copy(
                isActive = !employee.isActive
            )
        )
    }

}