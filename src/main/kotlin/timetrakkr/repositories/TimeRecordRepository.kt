package timetrakkr.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import timetrakkr.entities.TimeRecord

@Repository
interface TimeRecordRepository: CrudRepository<TimeRecord, Long>