package timetrakkr.utils

import timetrakkr.entities.Employee

object EntityGenerator {
    fun createEmployee(): Employee = Employee(
        firstName = "Brandon",
        lastName = "Cruz",
        employeeType = EmployeeTypeEnum.CE.value
    )
}