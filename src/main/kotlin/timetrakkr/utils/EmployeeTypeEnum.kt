package timetrakkr.utils

enum class EmployeeTypeEnum(val value: String) {
    PM("Project Manager"),
    CE("Contractor")
}