package timetrakkr.service

import timetrakkr.entities.Employee

interface EmployeeService {
    fun createEmployee(body: Employee): Employee
    fun updateEmployee(body: Employee, id: Long): Employee
    fun activateEmployee(body: Employee, id: Long): Employee

}