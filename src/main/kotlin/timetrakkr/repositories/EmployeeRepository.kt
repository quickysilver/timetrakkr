package timetrakkr.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import timetrakkr.entities.Employee

@Repository
    interface EmployeeRepository: JpaRepository<Employee, Long> {

    @Query(
        """
            SELECT CASE WHEN COUNT(er) > 0 THEN TRUE ELSE FALSE END
            FROM Employee er WHERE er.firstName = :firstName AND er.lastName = :lastName
        """
    )
    fun doesEmployeeExist(firstName: String, lastName: String): Boolean
}